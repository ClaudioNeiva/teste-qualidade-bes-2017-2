package br.ucsal.bes20172.testequalidade.lista02;

public class Questao02 {

	private static final int QTD_NUM = 5;

	public QuestoesHelper questoesHelper;

	public Questao02(QuestoesHelper questoesHelper) {
		this.questoesHelper = questoesHelper;
	}

	public void obterNumerosExibirInformadoEInvertido() {
		int[] vet = new int[QTD_NUM]; // {0, 0, 0, 0, 0}
		int[] vetInvertido = new int[QTD_NUM]; // {0, 0, 0, 0, 0}

		// Utilizar Helper nos metodos
		questoesHelper.obterNumeros(vet);
		inverterOrdemNumeros(vet, vetInvertido);
		questoesHelper.exibirVetores(vet, vetInvertido);
	}

	public void inverterOrdemNumeros(int[] vet, int[] vetInvertida) {
		System.out.println("foi chamado o m�todo inverterOrdemNumeros...");
		int aux = vet.length;
		for (int i = 0; i < vetInvertida.length; i++) {
			aux--;
			vetInvertida[i] = vet[aux];
		}
		throw new RuntimeException();
	}

}
