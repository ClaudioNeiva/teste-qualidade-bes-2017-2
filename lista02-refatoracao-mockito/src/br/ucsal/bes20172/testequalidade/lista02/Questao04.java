package br.ucsal.bes20172.testequalidade.lista02;

public class Questao04 {

	public static long calcularFatorial(long n) {
		if (n == 0) {
			return 1;
		}
		return n * calcularFatorial(n - 1);
	}

}
