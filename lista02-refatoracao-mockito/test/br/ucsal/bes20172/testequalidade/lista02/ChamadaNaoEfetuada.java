package br.ucsal.bes20172.testequalidade.lista02;

public class ChamadaNaoEfetuada extends Exception {

	private static final long serialVersionUID = 1L;

	public ChamadaNaoEfetuada(String message) {
		super(message);
	}
	
}
