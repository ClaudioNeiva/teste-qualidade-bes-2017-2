package br.ucsal.bes20172.testequalidade.lista02;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class Questao02Test {

	@Mock
	QuestoesHelper questoesHelper;

	Questao02 questao02;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		questoesHelper = Mockito.mock(QuestoesHelper.class);

		questao02 = new Questao02(questoesHelper);
		questao02 = Mockito.spy(questao02);
	}

	@Test
	public void obterNumerosExibirInformadoEInvertido() {

		Mockito.doNothing().when(questao02).inverterOrdemNumeros(Mockito.any(int[].class), Mockito.any(int[].class));

		questao02.obterNumerosExibirInformadoEInvertido();

		Mockito.verify(questoesHelper).obterNumeros(Mockito.any(int[].class));
		Mockito.verify(questao02).inverterOrdemNumeros(Mockito.any(int[].class), Mockito.any(int[].class));
		Mockito.verify(questoesHelper).exibirVetores(Mockito.any(int[].class), Mockito.any(int[].class));
	}

}
