package br.ucsal.bes20172.testequalidade.lista02;

import org.junit.Assert;
import org.junit.Test;

public class Questao04Test {

	@Test
	public void calcularFatorial3() {
		// Dados de entrada
		long n = 5;

		// Sa�da esperada
		long fatEsperado = 120;

		// Chamada do m�todo e obten��o resultado esperado
		long fatAtual = Questao04.calcularFatorial(n);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(fatEsperado, fatAtual);
	}

}
