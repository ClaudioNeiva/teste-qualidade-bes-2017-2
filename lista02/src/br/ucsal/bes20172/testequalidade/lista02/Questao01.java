package br.ucsal.bes20172.testequalidade.lista02;

import java.util.Scanner;

public class Questao01 {

	private static final int QTD_NUM = 5;

	public static void main(String[] args) {
		obterNumerosEncontrarMaior();
	}

	public static void obterNumerosEncontrarMaior() {
		int[] vet = new int[QTD_NUM];
		obterNumeros(vet);
		int maior = encontrarMaiorNumero(vet);
		exibirMaiorNumero(maior);
	}

	public static void obterNumeros(int[] vet) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe " + vet.length + " n�meros:");
		for (int i = 0; i < vet.length; i++) {
			vet[i] = sc.nextInt();
		}
	}

	public static int encontrarMaiorNumero(int[] vet) {
		int maior = vet[0];
		for (int i = 1; i < vet.length; i++) {
			if (vet[i] > maior) {
				maior = vet[i];
			}
		}
		return maior;
	}

	public static void exibirMaiorNumero(int maior) {
		System.out.println("Dentre os n�meros informados, o maior foi " + maior);
	}
}
