package br.ucsal.bes20172.testequalidade.lista02;

import java.util.Scanner;

public class Questao02 {

	private static final int QTD_NUM = 5;

	public static void main(String[] args) {
		obterInverterNumeros();
	}

	private static void obterInverterNumeros() {
		int vet[] = new int[QTD_NUM];
		int vetInvertida[] = new int[QTD_NUM];
		obterNumeros(vet);
		inverterOrdemNumeros(vet, vetInvertida);
		exibirVetores(vet, vetInvertida);
	}

	public static void obterNumeros(int vet[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Informe " + vet.length + " n�meros:");
		for (int i = 0; i < vet.length; i++) {
			vet[i] = sc.nextInt();
		}

	}

	public static void inverterOrdemNumeros(int[] vet, int[] vetInvertida) {
		int aux = vet.length;
		for (int i = 0; i < vetInvertida.length; i++) {
			aux--;
			vetInvertida[i] = vet[aux];
		}

	}

	public static void exibirVetores(int[] vet, int[] vetInvertida) {

		System.out.println("N�meros informados: ");
		for (int i = 0; i < vet.length; i++) {
			System.out.print(" " + vet[i]);
		}

		System.out.println("\nN�mero em ordem inversa: ");
		for (int i = 0; i < vetInvertida.length; i++) {
			System.out.print(" " + vetInvertida[i]);
		}
	}

}
