package br.ucsal.bes20172.testequalidade.lista02;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Test;

public class Questao01Test {

	private static final String QUEBRA_LINHA = System.getProperty("line.separator");

	@Test
	public void encontrarMaiorNumeroPositivosSegundo() {

		// Dados de entrada
		int vet[] = { 4, 8, 7, 4, 3 };

		// Sa�da esperada
		int maiorEsperado = 8;

		// Executar o m�todo que est� sendo testado e obter o resultado atual
		int maiorAtual = Questao01.encontrarMaiorNumero(vet);

		// Comparar o resultado esperado com o resultado atual
		assertEquals(maiorEsperado, maiorAtual);
	}

	@Test
	public void encontrarMaiorNumeroPositivosPrimeiro() {

		int vet[] = { 14, 8, 7, 4, 3 };

		int maiorEsperado = 14;

		int maiorAtual = Questao01.encontrarMaiorNumero(vet);

		assertEquals(maiorEsperado, maiorAtual);

	}

	@Test
	public void encontrarMaiorNumeroPositivosUltimo() {

		int vet[] = { 4, 8, 7, 4, 13 };

		int maiorEsperado = 13;

		int maiorAtual = Questao01.encontrarMaiorNumero(vet);

		assertEquals(maiorEsperado, maiorAtual);

	}

	@Test
	public void encontrarMaiorNumeroNegativosSegundo() {

		// Dados de entrada
		int vet[] = { -4, -2, -7, -6, -13 };

		// Sa�da esperada
		int maiorEsperado = -2;

		// Executar o m�todo que est� sendo testado e obter o resultado atual
		int maiorAtual = Questao01.encontrarMaiorNumero(vet);

		// Comparar o resultado esperado com o resultado atual
		assertEquals(maiorEsperado, maiorAtual);
	}

	@Test
	public void entradaDadosObtencao5Numeros() {
		// Dados de entrada
		String numerosEntrada = "548\n678\n3\n1\n2";

		// Resultado esperado
		int[] vetEsperado = { 548, 678, 3, 1, 2 };

		// Preparar o mecanismo de entrada de dados do sistema, substituindo o
		// mesmo por um fluxo falso de dados
		InputStream inFake = new ByteArrayInputStream(numerosEntrada.getBytes());
		System.setIn(inFake);

		// Executar o m�todo que est� sendo testado e obter o resultado atual
		int[] vetAtual = new int[5];
		Questao01.obterNumeros(vetAtual);

		// Comparar o resultado esperado com o resultado atual
		Assert.assertArrayEquals(vetEsperado, vetAtual);
	}

	@Test
	public void mensagemObtencao5Numeros() {
		// Dados de entrada
		String numerosEntrada = "548\n678\n3\n1\n2";

		// Resultado esperado
		String mensagemEsperada = "Informe 5 n�meros:" + QUEBRA_LINHA;

		// Preparar o mecanismo de entrada de dados do sistema, substituindo o
		// mesmo por um fluxo falso de dados
		InputStream inFake = new ByteArrayInputStream(numerosEntrada.getBytes());
		System.setIn(inFake);

		// Preparar o mecanismo de sa�da de dados do sistema para o console,
		// substituindo o
		// mesmo por um fluxo falso de destino de dados
		OutputStream outFake = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outFake));

		// Executar o m�todo que est� sendo testado e obter o resultado atual
		int[] vet = new int[5];
		Questao01.obterNumeros(vet);
		String mensagemAtual = outFake.toString();

		// Comparar o resultado esperado com o resultado atual
		Assert.assertEquals(mensagemEsperada, mensagemAtual);
	}

}
