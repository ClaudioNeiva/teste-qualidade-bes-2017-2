package br.ucsal.bes20172.testequalidade.lista02;

import static org.junit.Assert.assertArrayEquals;

import org.junit.Test;

import br.ucsal.bes20172.testequalidade.lista02.Questao02;

public class Questao02Test {

	@Test
	public void testInverterOrdemNumeros() {
		Questao02 q2 = new Questao02();
		int vetInvertido[] = { 2, 5, 12, 9, 7 };
		int vet[] = { 7, 9, 12, 5, 2 };
		int vet3[] = new int[5];

		q2.inverterOrdemNumeros(vet, vet3);

		assertArrayEquals(vetInvertido, vet3);

	}

}
