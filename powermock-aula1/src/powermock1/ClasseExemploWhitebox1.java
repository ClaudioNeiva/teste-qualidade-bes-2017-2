package powermock1;

public class ClasseExemploWhitebox1 {

	private int soma;

	private static int somaStatic;

	private static int valor1Static;

	private static int valor2Static;

	public int somarInstanciaPublic(int a, int b) {
		return a + b;
	}

	private int somarInstanciaPrivate(int a, int b) {
		return a + b;
	}

	private void somarInstanciaPrivateVoid(int a, int b) {
		soma = a + b;
	}

	private static void somarClassePrivateVoid(int a, int b) {
		somaStatic = a + b;
	}

	private static void somarClassePrivateVoidSemParametros() {
		somaStatic = valor1Static + valor2Static;
	}

}
