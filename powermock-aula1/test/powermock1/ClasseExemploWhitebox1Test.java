package powermock1;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.powermock.reflect.Whitebox;

public class ClasseExemploWhitebox1Test {

	private ClasseExemploWhitebox1 instancia1;

	@Before
	public void setup() {
		instancia1 = new ClasseExemploWhitebox1();
	}

	@Test
	public void testarsomaInstanciaPublic() {
		int a = 5;
		int b = 7;
		int somaEsperada = 12;
		int somaAtual = instancia1.somarInstanciaPublic(a, b);
		Assert.assertEquals(somaEsperada, somaAtual);
	}

	@Test
	public void testarSomaInstanciaPrivate() throws Exception {
		int a = 5;
		int b = 7;
		int somaEsperada = 12;
		int somaAtual = Whitebox.invokeMethod(instancia1, "somaInstanciaPrivate", a, b);
		Assert.assertEquals(somaEsperada, somaAtual);
	}

	@Test
	public void testarSomaInstanciaPrivateVoid() throws Exception {
		int a = 5;
		int b = 7;
		int somaEsperada = 12;
		Whitebox.invokeMethod(instancia1, "somaInstanciaPrivateVoid", a, b);
		int somaAtual = Whitebox.getInternalState(instancia1, "soma");
		Assert.assertEquals(somaEsperada, somaAtual);
	}

	@Test
	public void testarSomaClassePrivateVoid() throws Exception {
		int a = 5;
		int b = 7;
		int somaEsperada = 12;
		Whitebox.invokeMethod(ClasseExemploWhitebox1.class, "somaClassePrivateVoid", a, b);
		int somaAtual = Whitebox.getInternalState(ClasseExemploWhitebox1.class, "somaStatic");
		Assert.assertEquals(somaEsperada, somaAtual);
	}

	@Test
	public void testarSomaClassePrivateVoidSemParametros() throws Exception {
		int a = 5;
		int b = 7;
		int somaEsperada = 12;
		Whitebox.setInternalState(ClasseExemploWhitebox1.class, "valor1Static", a);
		Whitebox.setInternalState(ClasseExemploWhitebox1.class, "valor2Static", b);
		Whitebox.invokeMethod(ClasseExemploWhitebox1.class, "somaClassePrivateVoidSemParametros");
		int somaAtual = Whitebox.getInternalState(ClasseExemploWhitebox1.class, "somaStatic");
		Assert.assertEquals(somaEsperada, somaAtual);
	}
}
