package powermock1;

import org.junit.Assert;
import org.junit.Test;

public class PowermockExemplo1IntegradoTest {

	@Test
	public void somarTest() {
		int a = 1;
		int b = 5;
		int somaEsperada = 6;
		int somaAtual = PowermockExemplo1.somar(a, b);
		Assert.assertEquals(somaEsperada, somaAtual);
	}

}
