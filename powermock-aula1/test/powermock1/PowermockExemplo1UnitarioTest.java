package powermock1;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@PrepareForTest({ Servico.class })
@RunWith(PowerMockRunner.class)
public class PowermockExemplo1UnitarioTest {

	@Test
	public void somarTest() throws Exception {
		int a = 1;
		int b = 5;
		int somaEsperada = 6;

		// Mock em um m�todo est�tico, para garantir que a chamada ao m�todo
		// somar da classe Servico, retore um valor espec�fico em lugar de
		// executar a l�gica atual do referido m�todo.

		// PowerMockito.spy(Servico.class);
		// PowerMockito.doReturn(6).when(Servico.class, "somar", a, b);

		// PowerMockito.mockStatic(Servico.class);
		// PowerMockito.doCallRealMethod().when(Servico.class, "somar", a, b);

		PowerMockito.mockStatic(Servico.class);
		PowerMockito.doReturn(6).when(Servico.class, "somar", a, b);
		
		int somaAtual = PowermockExemplo1.somar(a, b);
		
		Assert.assertEquals(somaEsperada, somaAtual);
	}

}
